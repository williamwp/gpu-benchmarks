
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/cache_kernels.cu" "CMakeFiles/gpuroofperf-bench.dir/cache_kernels.cu.o" "gcc" "CMakeFiles/gpuroofperf-bench.dir/cache_kernels.cu.o.d"
  "/home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/computation_kernels.cu" "CMakeFiles/gpuroofperf-bench.dir/computation_kernels.cu.o" "gcc" "CMakeFiles/gpuroofperf-bench.dir/computation_kernels.cu.o.d"
  "/home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/dram_kernels.cu" "CMakeFiles/gpuroofperf-bench.dir/dram_kernels.cu.o" "gcc" "CMakeFiles/gpuroofperf-bench.dir/dram_kernels.cu.o.d"
  "/home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/shmem_kernels.cu" "CMakeFiles/gpuroofperf-bench.dir/shmem_kernels.cu.o" "gcc" "CMakeFiles/gpuroofperf-bench.dir/shmem_kernels.cu.o.d"
  "/home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/benchmark_manager.cpp" "CMakeFiles/gpuroofperf-bench.dir/benchmark_manager.cpp.o" "gcc" "CMakeFiles/gpuroofperf-bench.dir/benchmark_manager.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/main.cpp" "CMakeFiles/gpuroofperf-bench.dir/main.cpp.o" "gcc" "CMakeFiles/gpuroofperf-bench.dir/main.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
