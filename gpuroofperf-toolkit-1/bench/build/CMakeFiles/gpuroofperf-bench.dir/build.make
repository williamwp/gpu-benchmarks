# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.24

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /snap/cmake/1147/bin/cmake

# The command to remove a file.
RM = /snap/cmake/1147/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/build

# Include any dependencies generated for this target.
include CMakeFiles/gpuroofperf-bench.dir/depend.make
# Include any dependencies generated by the compiler for this target.
include CMakeFiles/gpuroofperf-bench.dir/compiler_depend.make

# Include the progress variables for this target.
include CMakeFiles/gpuroofperf-bench.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/gpuroofperf-bench.dir/flags.make

CMakeFiles/gpuroofperf-bench.dir/main.cpp.o: CMakeFiles/gpuroofperf-bench.dir/flags.make
CMakeFiles/gpuroofperf-bench.dir/main.cpp.o: /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/main.cpp
CMakeFiles/gpuroofperf-bench.dir/main.cpp.o: CMakeFiles/gpuroofperf-bench.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/gpuroofperf-bench.dir/main.cpp.o"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT CMakeFiles/gpuroofperf-bench.dir/main.cpp.o -MF CMakeFiles/gpuroofperf-bench.dir/main.cpp.o.d -o CMakeFiles/gpuroofperf-bench.dir/main.cpp.o -c /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/main.cpp

CMakeFiles/gpuroofperf-bench.dir/main.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/gpuroofperf-bench.dir/main.cpp.i"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/main.cpp > CMakeFiles/gpuroofperf-bench.dir/main.cpp.i

CMakeFiles/gpuroofperf-bench.dir/main.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/gpuroofperf-bench.dir/main.cpp.s"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/main.cpp -o CMakeFiles/gpuroofperf-bench.dir/main.cpp.s

CMakeFiles/gpuroofperf-bench.dir/benchmark_manager.cpp.o: CMakeFiles/gpuroofperf-bench.dir/flags.make
CMakeFiles/gpuroofperf-bench.dir/benchmark_manager.cpp.o: /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/benchmark_manager.cpp
CMakeFiles/gpuroofperf-bench.dir/benchmark_manager.cpp.o: CMakeFiles/gpuroofperf-bench.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Building CXX object CMakeFiles/gpuroofperf-bench.dir/benchmark_manager.cpp.o"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT CMakeFiles/gpuroofperf-bench.dir/benchmark_manager.cpp.o -MF CMakeFiles/gpuroofperf-bench.dir/benchmark_manager.cpp.o.d -o CMakeFiles/gpuroofperf-bench.dir/benchmark_manager.cpp.o -c /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/benchmark_manager.cpp

CMakeFiles/gpuroofperf-bench.dir/benchmark_manager.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/gpuroofperf-bench.dir/benchmark_manager.cpp.i"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/benchmark_manager.cpp > CMakeFiles/gpuroofperf-bench.dir/benchmark_manager.cpp.i

CMakeFiles/gpuroofperf-bench.dir/benchmark_manager.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/gpuroofperf-bench.dir/benchmark_manager.cpp.s"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/benchmark_manager.cpp -o CMakeFiles/gpuroofperf-bench.dir/benchmark_manager.cpp.s

CMakeFiles/gpuroofperf-bench.dir/computation_kernels.cu.o: CMakeFiles/gpuroofperf-bench.dir/flags.make
CMakeFiles/gpuroofperf-bench.dir/computation_kernels.cu.o: /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/computation_kernels.cu
CMakeFiles/gpuroofperf-bench.dir/computation_kernels.cu.o: CMakeFiles/gpuroofperf-bench.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Building CUDA object CMakeFiles/gpuroofperf-bench.dir/computation_kernels.cu.o"
	/usr/bin/nvcc  $(CUDA_DEFINES) $(CUDA_INCLUDES) $(CUDA_FLAGS) -x cu -c /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/computation_kernels.cu -o CMakeFiles/gpuroofperf-bench.dir/computation_kernels.cu.o
	/usr/bin/nvcc  $(CUDA_DEFINES) $(CUDA_INCLUDES) $(CUDA_FLAGS) -x cu -M /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/computation_kernels.cu -MT CMakeFiles/gpuroofperf-bench.dir/computation_kernels.cu.o -o CMakeFiles/gpuroofperf-bench.dir/computation_kernels.cu.o.d

CMakeFiles/gpuroofperf-bench.dir/computation_kernels.cu.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CUDA source to CMakeFiles/gpuroofperf-bench.dir/computation_kernels.cu.i"
	$(CMAKE_COMMAND) -E cmake_unimplemented_variable CMAKE_CUDA_CREATE_PREPROCESSED_SOURCE

CMakeFiles/gpuroofperf-bench.dir/computation_kernels.cu.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CUDA source to assembly CMakeFiles/gpuroofperf-bench.dir/computation_kernels.cu.s"
	$(CMAKE_COMMAND) -E cmake_unimplemented_variable CMAKE_CUDA_CREATE_ASSEMBLY_SOURCE

CMakeFiles/gpuroofperf-bench.dir/shmem_kernels.cu.o: CMakeFiles/gpuroofperf-bench.dir/flags.make
CMakeFiles/gpuroofperf-bench.dir/shmem_kernels.cu.o: /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/shmem_kernels.cu
CMakeFiles/gpuroofperf-bench.dir/shmem_kernels.cu.o: CMakeFiles/gpuroofperf-bench.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_4) "Building CUDA object CMakeFiles/gpuroofperf-bench.dir/shmem_kernels.cu.o"
	/usr/bin/nvcc  $(CUDA_DEFINES) $(CUDA_INCLUDES) $(CUDA_FLAGS) -x cu -c /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/shmem_kernels.cu -o CMakeFiles/gpuroofperf-bench.dir/shmem_kernels.cu.o
	/usr/bin/nvcc  $(CUDA_DEFINES) $(CUDA_INCLUDES) $(CUDA_FLAGS) -x cu -M /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/shmem_kernels.cu -MT CMakeFiles/gpuroofperf-bench.dir/shmem_kernels.cu.o -o CMakeFiles/gpuroofperf-bench.dir/shmem_kernels.cu.o.d

CMakeFiles/gpuroofperf-bench.dir/shmem_kernels.cu.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CUDA source to CMakeFiles/gpuroofperf-bench.dir/shmem_kernels.cu.i"
	$(CMAKE_COMMAND) -E cmake_unimplemented_variable CMAKE_CUDA_CREATE_PREPROCESSED_SOURCE

CMakeFiles/gpuroofperf-bench.dir/shmem_kernels.cu.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CUDA source to assembly CMakeFiles/gpuroofperf-bench.dir/shmem_kernels.cu.s"
	$(CMAKE_COMMAND) -E cmake_unimplemented_variable CMAKE_CUDA_CREATE_ASSEMBLY_SOURCE

CMakeFiles/gpuroofperf-bench.dir/dram_kernels.cu.o: CMakeFiles/gpuroofperf-bench.dir/flags.make
CMakeFiles/gpuroofperf-bench.dir/dram_kernels.cu.o: /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/dram_kernels.cu
CMakeFiles/gpuroofperf-bench.dir/dram_kernels.cu.o: CMakeFiles/gpuroofperf-bench.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_5) "Building CUDA object CMakeFiles/gpuroofperf-bench.dir/dram_kernels.cu.o"
	/usr/bin/nvcc  $(CUDA_DEFINES) $(CUDA_INCLUDES) $(CUDA_FLAGS) -x cu -c /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/dram_kernels.cu -o CMakeFiles/gpuroofperf-bench.dir/dram_kernels.cu.o
	/usr/bin/nvcc  $(CUDA_DEFINES) $(CUDA_INCLUDES) $(CUDA_FLAGS) -x cu -M /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/dram_kernels.cu -MT CMakeFiles/gpuroofperf-bench.dir/dram_kernels.cu.o -o CMakeFiles/gpuroofperf-bench.dir/dram_kernels.cu.o.d

CMakeFiles/gpuroofperf-bench.dir/dram_kernels.cu.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CUDA source to CMakeFiles/gpuroofperf-bench.dir/dram_kernels.cu.i"
	$(CMAKE_COMMAND) -E cmake_unimplemented_variable CMAKE_CUDA_CREATE_PREPROCESSED_SOURCE

CMakeFiles/gpuroofperf-bench.dir/dram_kernels.cu.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CUDA source to assembly CMakeFiles/gpuroofperf-bench.dir/dram_kernels.cu.s"
	$(CMAKE_COMMAND) -E cmake_unimplemented_variable CMAKE_CUDA_CREATE_ASSEMBLY_SOURCE

CMakeFiles/gpuroofperf-bench.dir/cache_kernels.cu.o: CMakeFiles/gpuroofperf-bench.dir/flags.make
CMakeFiles/gpuroofperf-bench.dir/cache_kernels.cu.o: /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/cache_kernels.cu
CMakeFiles/gpuroofperf-bench.dir/cache_kernels.cu.o: CMakeFiles/gpuroofperf-bench.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_6) "Building CUDA object CMakeFiles/gpuroofperf-bench.dir/cache_kernels.cu.o"
	/usr/bin/nvcc  $(CUDA_DEFINES) $(CUDA_INCLUDES) $(CUDA_FLAGS) -x cu -c /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/cache_kernels.cu -o CMakeFiles/gpuroofperf-bench.dir/cache_kernels.cu.o
	/usr/bin/nvcc  $(CUDA_DEFINES) $(CUDA_INCLUDES) $(CUDA_FLAGS) -x cu -M /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/cache_kernels.cu -MT CMakeFiles/gpuroofperf-bench.dir/cache_kernels.cu.o -o CMakeFiles/gpuroofperf-bench.dir/cache_kernels.cu.o.d

CMakeFiles/gpuroofperf-bench.dir/cache_kernels.cu.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CUDA source to CMakeFiles/gpuroofperf-bench.dir/cache_kernels.cu.i"
	$(CMAKE_COMMAND) -E cmake_unimplemented_variable CMAKE_CUDA_CREATE_PREPROCESSED_SOURCE

CMakeFiles/gpuroofperf-bench.dir/cache_kernels.cu.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CUDA source to assembly CMakeFiles/gpuroofperf-bench.dir/cache_kernels.cu.s"
	$(CMAKE_COMMAND) -E cmake_unimplemented_variable CMAKE_CUDA_CREATE_ASSEMBLY_SOURCE

# Object files for target gpuroofperf-bench
gpuroofperf__bench_OBJECTS = \
"CMakeFiles/gpuroofperf-bench.dir/main.cpp.o" \
"CMakeFiles/gpuroofperf-bench.dir/benchmark_manager.cpp.o" \
"CMakeFiles/gpuroofperf-bench.dir/computation_kernels.cu.o" \
"CMakeFiles/gpuroofperf-bench.dir/shmem_kernels.cu.o" \
"CMakeFiles/gpuroofperf-bench.dir/dram_kernels.cu.o" \
"CMakeFiles/gpuroofperf-bench.dir/cache_kernels.cu.o"

# External object files for target gpuroofperf-bench
gpuroofperf__bench_EXTERNAL_OBJECTS =

gpuroofperf-bench: CMakeFiles/gpuroofperf-bench.dir/main.cpp.o
gpuroofperf-bench: CMakeFiles/gpuroofperf-bench.dir/benchmark_manager.cpp.o
gpuroofperf-bench: CMakeFiles/gpuroofperf-bench.dir/computation_kernels.cu.o
gpuroofperf-bench: CMakeFiles/gpuroofperf-bench.dir/shmem_kernels.cu.o
gpuroofperf-bench: CMakeFiles/gpuroofperf-bench.dir/dram_kernels.cu.o
gpuroofperf-bench: CMakeFiles/gpuroofperf-bench.dir/cache_kernels.cu.o
gpuroofperf-bench: CMakeFiles/gpuroofperf-bench.dir/build.make
gpuroofperf-bench: CMakeFiles/gpuroofperf-bench.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_7) "Linking CXX executable gpuroofperf-bench"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/gpuroofperf-bench.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/gpuroofperf-bench.dir/build: gpuroofperf-bench
.PHONY : CMakeFiles/gpuroofperf-bench.dir/build

CMakeFiles/gpuroofperf-bench.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/gpuroofperf-bench.dir/cmake_clean.cmake
.PHONY : CMakeFiles/gpuroofperf-bench.dir/clean

CMakeFiles/gpuroofperf-bench.dir/depend:
	cd /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/build /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/build /home/test/tools/gpu-benchmarks/gpuroofperf-toolkit/bench/build/CMakeFiles/gpuroofperf-bench.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/gpuroofperf-bench.dir/depend

