
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/test/tools/gpu-benchmarks/comm_scope/thirdparty/libscope/src/do_not_optimize.cu" "thirdparty/libscope/CMakeFiles/scope.dir/src/do_not_optimize.cu.o" "gcc" "thirdparty/libscope/CMakeFiles/scope.dir/src/do_not_optimize.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/thirdparty/libscope/src/cache.cpp" "thirdparty/libscope/CMakeFiles/scope.dir/src/cache.cpp.o" "gcc" "thirdparty/libscope/CMakeFiles/scope.dir/src/cache.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/thirdparty/libscope/src/cuda.cpp" "thirdparty/libscope/CMakeFiles/scope.dir/src/cuda.cpp.o" "gcc" "thirdparty/libscope/CMakeFiles/scope.dir/src/cuda.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/thirdparty/libscope/src/flags.cpp" "thirdparty/libscope/CMakeFiles/scope.dir/src/flags.cpp.o" "gcc" "thirdparty/libscope/CMakeFiles/scope.dir/src/flags.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/thirdparty/libscope/src/governor.cpp" "thirdparty/libscope/CMakeFiles/scope.dir/src/governor.cpp.o" "gcc" "thirdparty/libscope/CMakeFiles/scope.dir/src/governor.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/thirdparty/libscope/src/init.cpp" "thirdparty/libscope/CMakeFiles/scope.dir/src/init.cpp.o" "gcc" "thirdparty/libscope/CMakeFiles/scope.dir/src/init.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/thirdparty/libscope/src/logger.cpp" "thirdparty/libscope/CMakeFiles/scope.dir/src/logger.cpp.o" "gcc" "thirdparty/libscope/CMakeFiles/scope.dir/src/logger.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/thirdparty/libscope/src/numa.cpp" "thirdparty/libscope/CMakeFiles/scope.dir/src/numa.cpp.o" "gcc" "thirdparty/libscope/CMakeFiles/scope.dir/src/numa.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/thirdparty/libscope/src/turbo.cpp" "thirdparty/libscope/CMakeFiles/scope.dir/src/turbo.cpp.o" "gcc" "thirdparty/libscope/CMakeFiles/scope.dir/src/turbo.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/test/tools/gpu-benchmarks/comm_scope/build/thirdparty/libscope/thirdparty/benchmark/src/CMakeFiles/benchmark.dir/DependInfo.cmake"
  "/home/test/tools/gpu-benchmarks/comm_scope/build/thirdparty/libscope/thirdparty/spdlog/CMakeFiles/spdlog.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
