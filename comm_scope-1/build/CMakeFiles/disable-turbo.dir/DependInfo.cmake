
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/test/tools/gpu-benchmarks/comm_scope/src/disable_turbo.cpp" "CMakeFiles/disable-turbo.dir/src/disable_turbo.cpp.o" "gcc" "CMakeFiles/disable-turbo.dir/src/disable_turbo.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/test/tools/gpu-benchmarks/comm_scope/build/thirdparty/libscope/CMakeFiles/scope.dir/DependInfo.cmake"
  "/home/test/tools/gpu-benchmarks/comm_scope/build/thirdparty/libscope/thirdparty/benchmark/src/CMakeFiles/benchmark.dir/DependInfo.cmake"
  "/home/test/tools/gpu-benchmarks/comm_scope/build/thirdparty/libscope/thirdparty/spdlog/CMakeFiles/spdlog.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
