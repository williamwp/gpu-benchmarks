# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.24

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /snap/cmake/1147/bin/cmake

# The command to remove a file.
RM = /snap/cmake/1147/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/test/tools/gpu-benchmarks/comm_scope

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/test/tools/gpu-benchmarks/comm_scope/build

# Include any dependencies generated for this target.
include CMakeFiles/disable-turbo.dir/depend.make
# Include any dependencies generated by the compiler for this target.
include CMakeFiles/disable-turbo.dir/compiler_depend.make

# Include the progress variables for this target.
include CMakeFiles/disable-turbo.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/disable-turbo.dir/flags.make

CMakeFiles/disable-turbo.dir/src/disable_turbo.cpp.o: CMakeFiles/disable-turbo.dir/flags.make
CMakeFiles/disable-turbo.dir/src/disable_turbo.cpp.o: /home/test/tools/gpu-benchmarks/comm_scope/src/disable_turbo.cpp
CMakeFiles/disable-turbo.dir/src/disable_turbo.cpp.o: CMakeFiles/disable-turbo.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/test/tools/gpu-benchmarks/comm_scope/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/disable-turbo.dir/src/disable_turbo.cpp.o"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT CMakeFiles/disable-turbo.dir/src/disable_turbo.cpp.o -MF CMakeFiles/disable-turbo.dir/src/disable_turbo.cpp.o.d -o CMakeFiles/disable-turbo.dir/src/disable_turbo.cpp.o -c /home/test/tools/gpu-benchmarks/comm_scope/src/disable_turbo.cpp

CMakeFiles/disable-turbo.dir/src/disable_turbo.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/disable-turbo.dir/src/disable_turbo.cpp.i"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/test/tools/gpu-benchmarks/comm_scope/src/disable_turbo.cpp > CMakeFiles/disable-turbo.dir/src/disable_turbo.cpp.i

CMakeFiles/disable-turbo.dir/src/disable_turbo.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/disable-turbo.dir/src/disable_turbo.cpp.s"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/test/tools/gpu-benchmarks/comm_scope/src/disable_turbo.cpp -o CMakeFiles/disable-turbo.dir/src/disable_turbo.cpp.s

# Object files for target disable-turbo
disable__turbo_OBJECTS = \
"CMakeFiles/disable-turbo.dir/src/disable_turbo.cpp.o"

# External object files for target disable-turbo
disable__turbo_EXTERNAL_OBJECTS =

CMakeFiles/disable-turbo.dir/cmake_device_link.o: CMakeFiles/disable-turbo.dir/src/disable_turbo.cpp.o
CMakeFiles/disable-turbo.dir/cmake_device_link.o: CMakeFiles/disable-turbo.dir/build.make
CMakeFiles/disable-turbo.dir/cmake_device_link.o: thirdparty/libscope/libscope.a
CMakeFiles/disable-turbo.dir/cmake_device_link.o: thirdparty/libscope/thirdparty/benchmark/src/libbenchmark.a
CMakeFiles/disable-turbo.dir/cmake_device_link.o: /usr/lib/x86_64-linux-gnu/librt.so
CMakeFiles/disable-turbo.dir/cmake_device_link.o: thirdparty/libscope/thirdparty/spdlog/libspdlog.a
CMakeFiles/disable-turbo.dir/cmake_device_link.o: /usr/lib/x86_64-linux-gnu/libcudart.so
CMakeFiles/disable-turbo.dir/cmake_device_link.o: /usr/lib/x86_64-linux-gnu/libnvToolsExt.so
CMakeFiles/disable-turbo.dir/cmake_device_link.o: CMakeFiles/disable-turbo.dir/dlink.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/test/tools/gpu-benchmarks/comm_scope/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CUDA device code CMakeFiles/disable-turbo.dir/cmake_device_link.o"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/disable-turbo.dir/dlink.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/disable-turbo.dir/build: CMakeFiles/disable-turbo.dir/cmake_device_link.o
.PHONY : CMakeFiles/disable-turbo.dir/build

# Object files for target disable-turbo
disable__turbo_OBJECTS = \
"CMakeFiles/disable-turbo.dir/src/disable_turbo.cpp.o"

# External object files for target disable-turbo
disable__turbo_EXTERNAL_OBJECTS =

disable-turbo: CMakeFiles/disable-turbo.dir/src/disable_turbo.cpp.o
disable-turbo: CMakeFiles/disable-turbo.dir/build.make
disable-turbo: thirdparty/libscope/libscope.a
disable-turbo: thirdparty/libscope/thirdparty/benchmark/src/libbenchmark.a
disable-turbo: /usr/lib/x86_64-linux-gnu/librt.so
disable-turbo: thirdparty/libscope/thirdparty/spdlog/libspdlog.a
disable-turbo: /usr/lib/x86_64-linux-gnu/libcudart.so
disable-turbo: /usr/lib/x86_64-linux-gnu/libnvToolsExt.so
disable-turbo: CMakeFiles/disable-turbo.dir/cmake_device_link.o
disable-turbo: CMakeFiles/disable-turbo.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/test/tools/gpu-benchmarks/comm_scope/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Linking CXX executable disable-turbo"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/disable-turbo.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/disable-turbo.dir/build: disable-turbo
.PHONY : CMakeFiles/disable-turbo.dir/build

CMakeFiles/disable-turbo.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/disable-turbo.dir/cmake_clean.cmake
.PHONY : CMakeFiles/disable-turbo.dir/clean

CMakeFiles/disable-turbo.dir/depend:
	cd /home/test/tools/gpu-benchmarks/comm_scope/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/test/tools/gpu-benchmarks/comm_scope /home/test/tools/gpu-benchmarks/comm_scope /home/test/tools/gpu-benchmarks/comm_scope/build /home/test/tools/gpu-benchmarks/comm_scope/build /home/test/tools/gpu-benchmarks/comm_scope/build/CMakeFiles/disable-turbo.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/disable-turbo.dir/depend

