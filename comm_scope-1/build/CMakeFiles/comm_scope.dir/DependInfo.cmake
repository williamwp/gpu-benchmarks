
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/test/tools/gpu-benchmarks/comm_scope/src/3d/kernel3D/gpu_to_pinned.cu" "CMakeFiles/comm_scope.dir/src/3d/kernel3D/gpu_to_pinned.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/3d/kernel3D/gpu_to_pinned.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/3d/kernel3D/pinned_to_gpu.cu" "CMakeFiles/comm_scope.dir/src/3d/kernel3D/pinned_to_gpu.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/3d/kernel3D/pinned_to_gpu.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/3d/kernel3D/pull.cu" "CMakeFiles/comm_scope.dir/src/3d/kernel3D/pull.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/3d/kernel3D/pull.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/3d/kernel3D/push.cu" "CMakeFiles/comm_scope.dir/src/3d/kernel3D/push.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/3d/kernel3D/push.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/3d/pack_cudaMemcpyPeerAsync_unpack.cu" "CMakeFiles/comm_scope.dir/src/3d/pack_cudaMemcpyPeerAsync_unpack.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/3d/pack_cudaMemcpyPeerAsync_unpack.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/chunk/pull.cu" "CMakeFiles/comm_scope.dir/src/chunk/pull.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/chunk/pull.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/chunk/push.cu" "CMakeFiles/comm_scope.dir/src/chunk/push.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/chunk/push.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/cudaMemcpyPeerAsync-duplex/gpu_gpu.cu" "CMakeFiles/comm_scope.dir/src/cudaMemcpyPeerAsync-duplex/gpu_gpu.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/cudaMemcpyPeerAsync-duplex/gpu_gpu.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/cudaMemcpyPeerAsync-duplex/gpu_gpu_peer.cu" "CMakeFiles/comm_scope.dir/src/cudaMemcpyPeerAsync-duplex/gpu_gpu_peer.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/cudaMemcpyPeerAsync-duplex/gpu_gpu_peer.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/demand-duplex/gpu_gpu.cu" "CMakeFiles/comm_scope.dir/src/demand-duplex/gpu_gpu.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/demand-duplex/gpu_gpu.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/demand-duplex/host_gpu.cu" "CMakeFiles/comm_scope.dir/src/demand-duplex/host_gpu.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/demand-duplex/host_gpu.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/latency/gpu_to_gpu.cu" "CMakeFiles/comm_scope.dir/src/latency/gpu_to_gpu.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/latency/gpu_to_gpu.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/latency/gpu_to_host.cu" "CMakeFiles/comm_scope.dir/src/latency/gpu_to_host.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/latency/gpu_to_host.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/latency/host_to_gpu.cu" "CMakeFiles/comm_scope.dir/src/latency/host_to_gpu.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/latency/host_to_gpu.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/libcudart/cudaGraphInstantiate_cudaMemcpyAsync.cu" "CMakeFiles/comm_scope.dir/src/libcudart/cudaGraphInstantiate_cudaMemcpyAsync.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/libcudart/cudaGraphInstantiate_cudaMemcpyAsync.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/libcudart/cudaGraphInstantiate_kernel.cu" "CMakeFiles/comm_scope.dir/src/libcudart/cudaGraphInstantiate_kernel.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/libcudart/cudaGraphInstantiate_kernel.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/libcudart/cudaGraphLaunch_cudaMemcpy3DAsync.cu" "CMakeFiles/comm_scope.dir/src/libcudart/cudaGraphLaunch_cudaMemcpy3DAsync.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/libcudart/cudaGraphLaunch_cudaMemcpy3DAsync.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/libcudart/cudaGraphLaunch_cudaMemcpyAsync.cu" "CMakeFiles/comm_scope.dir/src/libcudart/cudaGraphLaunch_cudaMemcpyAsync.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/libcudart/cudaGraphLaunch_cudaMemcpyAsync.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/libcudart/cudaGraphLaunch_kernel.cu" "CMakeFiles/comm_scope.dir/src/libcudart/cudaGraphLaunch_kernel.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/libcudart/cudaGraphLaunch_kernel.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/libcudart/kernel.cu" "CMakeFiles/comm_scope.dir/src/libcudart/kernel.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/libcudart/kernel.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/prefetch-duplex/gpu_gpu.cu" "CMakeFiles/comm_scope.dir/src/prefetch-duplex/gpu_gpu.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/prefetch-duplex/gpu_gpu.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/prefetch-duplex/host_gpu.cu" "CMakeFiles/comm_scope.dir/src/prefetch-duplex/host_gpu.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/prefetch-duplex/host_gpu.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/prefetch/gpu_to_gpu.cu" "CMakeFiles/comm_scope.dir/src/prefetch/gpu_to_gpu.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/prefetch/gpu_to_gpu.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/prefetch/gpu_to_host.cu" "CMakeFiles/comm_scope.dir/src/prefetch/gpu_to_host.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/prefetch/gpu_to_host.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/prefetch/host_to_gpu.cu" "CMakeFiles/comm_scope.dir/src/prefetch/host_to_gpu.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/prefetch/host_to_gpu.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/stride/pull.cu" "CMakeFiles/comm_scope.dir/src/stride/pull.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/stride/pull.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/stride/push.cu" "CMakeFiles/comm_scope.dir/src/stride/push.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/stride/push.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/zerocopy-duplex/gpu_gpu.cu" "CMakeFiles/comm_scope.dir/src/zerocopy-duplex/gpu_gpu.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/zerocopy-duplex/gpu_gpu.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/zerocopy-duplex/host_gpu.cu" "CMakeFiles/comm_scope.dir/src/zerocopy-duplex/host_gpu.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/zerocopy-duplex/host_gpu.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/zerocopy/gpu_to_gpu.cu" "CMakeFiles/comm_scope.dir/src/zerocopy/gpu_to_gpu.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/zerocopy/gpu_to_gpu.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/zerocopy/gpu_to_host.cu" "CMakeFiles/comm_scope.dir/src/zerocopy/gpu_to_host.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/zerocopy/gpu_to_host.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/zerocopy/host_to_gpu.cu" "CMakeFiles/comm_scope.dir/src/zerocopy/host_to_gpu.cu.o" "gcc" "CMakeFiles/comm_scope.dir/src/zerocopy/host_to_gpu.cu.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/3d/cudaMemcpy2DAsync/gpu_to_gpu.cpp" "CMakeFiles/comm_scope.dir/src/3d/cudaMemcpy2DAsync/gpu_to_gpu.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/3d/cudaMemcpy2DAsync/gpu_to_gpu.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/3d/cudaMemcpy2DAsync/gpu_to_pinned.cpp" "CMakeFiles/comm_scope.dir/src/3d/cudaMemcpy2DAsync/gpu_to_pinned.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/3d/cudaMemcpy2DAsync/gpu_to_pinned.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/3d/cudaMemcpy2DAsync/pinned_to_gpu.cpp" "CMakeFiles/comm_scope.dir/src/3d/cudaMemcpy2DAsync/pinned_to_gpu.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/3d/cudaMemcpy2DAsync/pinned_to_gpu.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/3d/cudaMemcpy3DAsync/gpu_to_pinned.cpp" "CMakeFiles/comm_scope.dir/src/3d/cudaMemcpy3DAsync/gpu_to_pinned.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/3d/cudaMemcpy3DAsync/gpu_to_pinned.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/3d/cudaMemcpy3DAsync/pinned_to_gpu.cpp" "CMakeFiles/comm_scope.dir/src/3d/cudaMemcpy3DAsync/pinned_to_gpu.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/3d/cudaMemcpy3DAsync/pinned_to_gpu.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/3d/cudaMemcpy3DPeerAsync/gpu_to_gpu.cpp" "CMakeFiles/comm_scope.dir/src/3d/cudaMemcpy3DPeerAsync/gpu_to_gpu.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/3d/cudaMemcpy3DPeerAsync/gpu_to_gpu.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/cudaMemcpyAsync-duplex/gpu_gpu_peer.cpp" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync-duplex/gpu_gpu_peer.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync-duplex/gpu_gpu_peer.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/cudaMemcpyAsync-duplex/host.cpp" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync-duplex/host.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync-duplex/host.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/cudaMemcpyAsync-duplex/pinned.cpp" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync-duplex/pinned.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync-duplex/pinned.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/cudaMemcpyAsync/gpu_to_gpu_nopeer.cpp" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync/gpu_to_gpu_nopeer.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync/gpu_to_gpu_nopeer.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/cudaMemcpyAsync/gpu_to_gpu_peer.cpp" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync/gpu_to_gpu_peer.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync/gpu_to_gpu_peer.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/cudaMemcpyAsync/gpu_to_host.cpp" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync/gpu_to_host.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync/gpu_to_host.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/cudaMemcpyAsync/gpu_to_pinned.cpp" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync/gpu_to_pinned.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync/gpu_to_pinned.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/cudaMemcpyAsync/gpu_to_wc.cpp" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync/gpu_to_wc.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync/gpu_to_wc.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/cudaMemcpyAsync/host_to_gpu.cpp" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync/host_to_gpu.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync/host_to_gpu.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/cudaMemcpyAsync/host_to_pinned.cpp" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync/host_to_pinned.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync/host_to_pinned.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/cudaMemcpyAsync/pinned_to_gpu.cpp" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync/pinned_to_gpu.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync/pinned_to_gpu.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/cudaMemcpyAsync/wc_to_gpu.cpp" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync/wc_to_gpu.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/cudaMemcpyAsync/wc_to_gpu.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/cudaMemcpyPeerAsync/gpu_to_gpu.cpp" "CMakeFiles/comm_scope.dir/src/cudaMemcpyPeerAsync/gpu_to_gpu.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/cudaMemcpyPeerAsync/gpu_to_gpu.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/cudaMemcpyPeerAsync/gpu_to_gpu_peer.cpp" "CMakeFiles/comm_scope.dir/src/cudaMemcpyPeerAsync/gpu_to_gpu_peer.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/cudaMemcpyPeerAsync/gpu_to_gpu_peer.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/libc/malloc.cpp" "CMakeFiles/comm_scope.dir/src/libc/malloc.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/libc/malloc.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/libc/malloc_memset.cpp" "CMakeFiles/comm_scope.dir/src/libc/malloc_memset.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/libc/malloc_memset.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/libcudart/cudaMallocManaged.cpp" "CMakeFiles/comm_scope.dir/src/libcudart/cudaMallocManaged.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/libcudart/cudaMallocManaged.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/libcudart/cudaMemcpy3DAsync_gpu_to_gpu.cpp" "CMakeFiles/comm_scope.dir/src/libcudart/cudaMemcpy3DAsync_gpu_to_gpu.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/libcudart/cudaMemcpy3DAsync_gpu_to_gpu.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/libcudart/cudaMemcpy3DPeerAsync.cpp" "CMakeFiles/comm_scope.dir/src/libcudart/cudaMemcpy3DPeerAsync.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/libcudart/cudaMemcpy3DPeerAsync.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/libcudart/cudaMemcpyAsync_gpu_to_pinned.cpp" "CMakeFiles/comm_scope.dir/src/libcudart/cudaMemcpyAsync_gpu_to_pinned.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/libcudart/cudaMemcpyAsync_gpu_to_pinned.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/libcudart/cudaMemcpyAsync_pinned_to_gpu.cpp" "CMakeFiles/comm_scope.dir/src/libcudart/cudaMemcpyAsync_pinned_to_gpu.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/libcudart/cudaMemcpyAsync_pinned_to_gpu.cpp.o.d"
  "/home/test/tools/gpu-benchmarks/comm_scope/src/main.cpp" "CMakeFiles/comm_scope.dir/src/main.cpp.o" "gcc" "CMakeFiles/comm_scope.dir/src/main.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/test/tools/gpu-benchmarks/comm_scope/build/thirdparty/libscope/CMakeFiles/scope.dir/DependInfo.cmake"
  "/home/test/tools/gpu-benchmarks/comm_scope/build/thirdparty/libscope/thirdparty/benchmark/src/CMakeFiles/benchmark.dir/DependInfo.cmake"
  "/home/test/tools/gpu-benchmarks/comm_scope/build/thirdparty/libscope/thirdparty/spdlog/CMakeFiles/spdlog.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
